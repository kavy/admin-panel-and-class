from django.contrib import admin
from django.db import models
from superman.models import language
from superman.models import student
admin.site.register(language)
admin.site.register(student)
