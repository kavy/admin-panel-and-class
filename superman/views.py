from django.shortcuts import render_to_response 
from django.template import RequestContext
from superman.models import language
from superman.models import student


def home(request):
	ctx=RequestContext(request)
	temp_name="index.html"
	response=render_to_response(temp_name,locals(),ctx)
	return response

def next(request):
	ctx=RequestContext(request)
	temp_name="page2.html"
	values=student.objects.all()
	response=render_to_response(temp_name,locals(),ctx)
	return response
	
def first(request):
 ctx=RequestContext(request)
 temp_name="mypage.html"
 values=language.objects.all()
 response=render_to_response(temp_name,locals(),ctx)
 return response


def second(request):
	ctx=RequestContext(request)
	temp_name="index.html"
	response=render_to_response(temp_name,{},ctx)
	return response